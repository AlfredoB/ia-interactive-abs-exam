using IA_interactive_ABS_exam_net.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IA_interactive_ABS_exam_net.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {

        private readonly ILogger<HomeController> _logger;
        private DbEntitiesContext _dbContext;

        public HomeController(ILogger<HomeController> logger, DbEntitiesContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
        }

        [HttpGet(Name = "/")]
        public String Get()
        {
            var listStatus = _dbContext.Status.ToList();
            if (listStatus.Count == 0)
            {
                _dbContext.Status.Add(new Status()
                {
                    Decription = "Active"
                });
                _dbContext.Status.Add(new Status()
                {
                    Decription = "Inactive"
                });

                _dbContext.Product.Add(new Product()
                {
                    SKU = "0001B",
                    Decription = "Burgers",
                    Stock = 2,
                    StatusID = 1,
                    TimeStamp = DateTime.Now
                });
                _dbContext.Product.Add(new Product()
                {
                    SKU = "0002S",
                    Decription = "Soda",
                    Stock = 2,
                    StatusID = 1,
                    TimeStamp = DateTime.Now
                });


                _dbContext.OrderStatus.Add(new OrderStatus()
                {
                    Decription = "Pending"
                });
                _dbContext.OrderStatus.Add(new OrderStatus()
                {
                    Decription = "In Process"
                });
                _dbContext.OrderStatus.Add(new OrderStatus()
                {
                    Decription = "Completed"
                });
                _dbContext.OrderStatus.Add(new OrderStatus()
                {
                    Decription = "Delivered"
                });
                _dbContext.OrderStatus.Add(new OrderStatus()
                {
                    Decription = "Canceled"
                });

                //Insert one order
                _dbContext.Order.Add(new Order()
                {
                    OrderStatusID = 1,
                    TimeStamp = DateTime.Now,
                    ProductsOrder = new List<ProductOrder>()
                    {
                        new ProductOrder() { ProductID = 1, Count = 1, OrderStatusID = 1},
                        new ProductOrder() { ProductID = 2, Count = 2, OrderStatusID = 1},
                    }
                });


                _dbContext.SaveChanges();
                return "API Client Initialized!";
            }

            return "API Client failed to inizalize";
        }
    }
}