using AutoMapper;
using IA_interactive_ABS_exam_net.Entities;
using IA_interactive_ABS_exam_net.Models;
using IA_interactive_ABS_exam_net.Parameters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IA_interactive_ABS_exam_net.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrderController : ControllerBase
    {

        private readonly ILogger<OrderController> _logger;
        private readonly IMapper _mapper;
        private DbEntitiesContext _dbContext;

        public OrderController(ILogger<OrderController> logger, IMapper mapper, DbEntitiesContext dbContext)
        {
            _logger = logger;
            _mapper = mapper;
            _dbContext = dbContext;
        }

        [HttpGet(Name = "GetOrders")]
        public ActionResult<List<DtoOrder>> Get()
        {
            var Orders = new List<DtoOrder>();

            try
            {
                Orders = _mapper.Map<List<DtoOrder>>(_dbContext.Order.Include("OrderStatus").Include("ProductsOrder").Include("ProductsOrder.Product").ToList());

                return StatusCode(StatusCodes.Status200OK, Orders);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Internal Error to get Orders");
            }
        }

        [HttpGet(Name = "GetOrdersByStatus")]
        [Route("Status/{OrderStatusID}")]
        public ActionResult<List<DtoOrder>> Get(int orderStatusID)
        {
            var Orders = new List<DtoOrder>();

            try
            {
                Orders = _mapper.Map<List<DtoOrder>>(_dbContext.Order.Where(o => o.OrderStatusID == orderStatusID).Include("OrderStatus").Include("ProductsOrder").Include("ProductsOrder.Product").ToList());

                return StatusCode(StatusCodes.Status200OK, Orders);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Internal Error to get Orders");
            }
        }

        [HttpPost(Name = "InsertOrder")]
        public ActionResult<string> Post([FromBody] OrderPM OrderPM)
        {
            try
            {
                Order order = new Order()
                {
                    OrderStatusID = 1,
                    TimeStamp = DateTime.Now,
                    ProductsOrder = new List<ProductOrder>()
                };


                foreach(var productOrder in OrderPM.ProductsOrder)
                {
                    var dbProduct = _dbContext.Product.FirstOrDefault(x => x.ProductID == productOrder.ProductID);

                    if(dbProduct == null)
                        return StatusCode(StatusCodes.Status202Accepted, String.Format("Product with ID: {0}, is not exist", productOrder.ProductID));

                    if (dbProduct.Stock < productOrder.Count)
                        return StatusCode(StatusCodes.Status202Accepted, String.Format("Stock the product: {0}, is insufficient", dbProduct.Decription));

                    dbProduct.Stock -= productOrder.Count;
                    _dbContext.Product.Attach(dbProduct);
                    _dbContext.Entry(dbProduct).State = EntityState.Modified;

                    order.ProductsOrder.Add(new ProductOrder() { Count = productOrder.Count, ProductID = productOrder.ProductID, OrderStatusID = 1 });
                }

                _dbContext.Order.Add(order);
                 _dbContext.SaveChanges();

                return StatusCode(StatusCodes.Status201Created, String.Format("Order inserted with ID: {0}", order.OrderID));
            }
            catch (Exception ex)
            {
               return StatusCode(StatusCodes.Status500InternalServerError, "Internal Error to create order");
            }
        }

        [HttpPut(Name = "UpdateOrder")]
        public ActionResult<string> Put([FromBody] OrderPM DtoOrder)
        {
            try
            {
                var ord = _dbContext.Order.FirstOrDefault(x => x.OrderID == DtoOrder.OrderID);

                if (ord == null)
                    return StatusCode(StatusCodes.Status202Accepted, "There aren't any Orders with that ID");

                ord.TimeStamp = DateTime.Now;

                _dbContext.Order.Attach(ord);
                _dbContext.Entry(ord).State = EntityState.Modified;
                _dbContext.SaveChanges();
                return StatusCode(StatusCodes.Status201Created, String.Format("Order with ID: {0}, was updated!", ord.OrderID));

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Internal Error to update order");
            }
        }

        [HttpPut(Name = "UpdateStatusOrder")]
        [Route("Status")]
        public ActionResult<string> Status([FromBody] OrderPM OrderPM)
        {
            try
            {
                var ord = _dbContext.Order.FirstOrDefault(x => x.OrderID == OrderPM.OrderID);

                if (ord == null)
                    return StatusCode(StatusCodes.Status202Accepted, "There aren't any Orders with that ID");

                //validation Manual
                if (_dbContext.OrderStatus.FirstOrDefault(x => x.OrderStatusID == OrderPM.OrderStatusID) == null)
                    return StatusCode(StatusCodes.Status202Accepted, "There aren't any Status of Order with that ID");

                ord.OrderStatusID = (int)OrderPM.OrderStatusID;
                ord.TimeStamp = DateTime.Now;

                _dbContext.Order.Attach(ord);
                _dbContext.Entry(ord).State = EntityState.Modified;
                _dbContext.SaveChanges();
                return StatusCode(StatusCodes.Status201Created, String.Format("Order with ID: {0}, was updated!", ord.OrderID));

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Internal Error to update status order");
            }
        }
    }
}