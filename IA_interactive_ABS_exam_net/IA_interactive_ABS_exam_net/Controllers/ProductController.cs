using IA_interactive_ABS_exam_net.Entities;
using IA_interactive_ABS_exam_net.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace IA_interactive_ABS_exam_net.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductController : ControllerBase
    {
        
        private readonly ILogger<ProductController> _logger;
        private readonly IMapper _mapper;
        private DbEntitiesContext _dbContext;

        public ProductController(ILogger<ProductController> logger, IMapper mapper, DbEntitiesContext dbContext)
        {
            _logger = logger;
            _mapper = mapper;
            _dbContext = dbContext; 
        }

        [HttpGet(Name = "GetProducts")]
        public ActionResult<DtoProduct> Get()
        {
            var Products = new List<DtoProduct>();

            try
            {
                Products = _mapper.Map<List<DtoProduct>>(_dbContext.Product.Include("Status").ToList());

                return StatusCode(StatusCodes.Status200OK, Products);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Internal Error to get Products");
            }

        }

        [HttpPost(Name = "InsertProduct")]
        public ActionResult<string> Post([FromBody] DtoProduct product)
        {
            try
            {
                if (_dbContext.Product.FirstOrDefault(x => product.SKU.ToLower().Equals(x.SKU.ToLower())) == null)
                {
                    var prod = _mapper.Map<Product>(product);
                    prod.TimeStamp = DateTime.Now;
                    _dbContext.Product.Add(prod);
                    _dbContext.SaveChanges();
                    return StatusCode(StatusCodes.Status201Created, String.Format("Product inserted with ID: {0}", prod.ProductID));
                }
                else
                {
                    return StatusCode(StatusCodes.Status202Accepted, String.Format("Product with SKU: {0}, is already exist", product.SKU));
                }
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Internal Error to insert Product");
            }
        }

        [HttpPut(Name = "UpdateProduct")]
        public ActionResult<string> Put([FromBody] DtoProduct DtoProduct)
        {
            try
            {
                var prod = _dbContext.Product.FirstOrDefault(x => x.ProductID == DtoProduct.ProductID);

                if (prod == null)
                    return StatusCode(StatusCodes.Status202Accepted, "There aren't any Products with that ID");

                if (_dbContext.Product.FirstOrDefault(x => DtoProduct.SKU.ToLower().Equals(x.SKU.ToLower())) == null &&
                    DtoProduct.ProductID != prod.ProductID)
                    return StatusCode(StatusCodes.Status202Accepted, String.Format("Product with SKU: {0}, is already exist", DtoProduct.SKU));

                prod.SKU = DtoProduct.SKU;
                prod.Decription = DtoProduct.Decription;
                prod.Stock = DtoProduct.Stock;
                prod.StatusID = DtoProduct.StatusID;
                prod.TimeStamp = DateTime.Now;

                _dbContext.Product.Attach(prod);
                _dbContext.Entry(prod).State = EntityState.Modified;
                _dbContext.SaveChanges();
                return StatusCode(StatusCodes.Status200OK, String.Format("Product with ID: {0}, was updated!", prod.ProductID));

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Internal Error to update Product");
            }
        }

        [HttpDelete(Name = "DeleteProduct")]
        public ActionResult<string> Delete(int productID)
        {
            try
            {
                var prod = _dbContext.Product.FirstOrDefault(x => x.ProductID == productID);

                if (prod == null)
                    return StatusCode(StatusCodes.Status202Accepted, "There aren't any Products with that ID");

                _dbContext.Product.Remove(prod);

                _dbContext.SaveChanges();
                return StatusCode(StatusCodes.Status200OK, String.Format("Product with ID: {0}, was removed!", productID));

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Internal Error to delete Product");
            }
        }
    }
}