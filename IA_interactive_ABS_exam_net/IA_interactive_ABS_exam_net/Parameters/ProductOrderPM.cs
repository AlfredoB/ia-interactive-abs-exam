﻿namespace IA_interactive_ABS_exam_net.Parameters
{
    public class ProductOrderPM
    {
        public int ProductOrderID { get; set; }
        public int? OrderID { get; set; }
        public int Count { get; set; }
        public int ProductID { get; set; }
        public int? OrderStatusID { get; set; }
    }
}
