﻿namespace IA_interactive_ABS_exam_net.Parameters
{
    public class OrderPM
    {
        public int? OrderID { get; set; }
        public int? OrderStatusID { get; set; }
        public List<ProductOrderPM>? ProductsOrder { get; set; }
    }
}
