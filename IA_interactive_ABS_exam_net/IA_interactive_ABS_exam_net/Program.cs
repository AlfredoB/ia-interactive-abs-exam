using IA_interactive_ABS_exam_net.Entities;
using IA_interactive_ABS_exam_net.Mapper;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddCors();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddDbContext<DbEntitiesContext>();
builder.Services.AddAutoMapper(typeof(MappingProfile));

var app = builder.Build();

app.UseCors(opt => {
    opt.WithOrigins("http://localhost:55382");
    opt.AllowAnyMethod();
    opt.AllowAnyHeader();
    }
);

app.UseAuthorization();

app.MapControllers();

app.Run();
