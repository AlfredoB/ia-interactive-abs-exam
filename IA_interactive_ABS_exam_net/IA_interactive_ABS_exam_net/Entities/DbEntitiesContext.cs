﻿using Microsoft.EntityFrameworkCore;

namespace IA_interactive_ABS_exam_net.Entities
{
    public class DbEntitiesContext : DbContext
    {
        public DbEntitiesContext(DbContextOptions<DbEntitiesContext> options)
            : base(options)
        { }
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderStatus> OrderStatus { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProductOrder> ProductOrder { get; set; }
        public DbSet<Status> Status { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase(databaseName: "TemporalDbContext");
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Product>()
                .HasIndex(u => u.SKU)
                .IsUnique();

            builder.Entity<Product>()
                .HasOne(p => p.Status)
                .WithMany(b => b.Products)
                .HasForeignKey(f => f.StatusID);

            builder.Entity<ProductOrder>()
                .HasOne(p => p.Order)
                .WithMany(b => b.ProductsOrder)
                .HasForeignKey(f => f.OrderID);

            builder.Entity<OrderStatus>()
                .HasMany(p => p.ProductsOrder);

            builder.Entity<Order>()
                .HasMany(p => p.ProductsOrder);

            builder.Entity<Order>()
                .HasOne(p => p.OrderStatus)
                .WithMany(b => b.Order)
                .HasForeignKey(f => f.OrderStatusID)
                .OnDelete(DeleteBehavior.ClientSetNull);
        }
    }
}
