using System.ComponentModel.DataAnnotations;

namespace IA_interactive_ABS_exam_net.Entities
{
    public class Product
    {

        [Key]
        public int ProductID { get; set; }
        [Required]
        public string SKU { get; set; }
        [Required]
        public string Decription { get; set; }
        [Required]
        public int Stock { get; set; }
        [Required]
        public int StatusID { get; set; }
        public DateTime TimeStamp { get; set; }

        //Relation externals
        public List<ProductOrder> ProductsOrder { get; set; }
        public Status Status { get; set; }
    }
}