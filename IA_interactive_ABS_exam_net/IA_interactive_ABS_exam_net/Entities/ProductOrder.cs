using System.ComponentModel.DataAnnotations;

namespace IA_interactive_ABS_exam_net.Entities
{
    public class ProductOrder
    {

        [Key]
        public int ProductOrderID { get; set; }
        public int OrderID { get; set; }
        public int ProductID { get; set; }
        public int Count { get; set; }
        public int OrderStatusID { get; set; }

        //Relation externals
        public Order Order { get; set; }
        public Product Product { get; set; }
        public OrderStatus OrderStatus { get; set; }
    }
}