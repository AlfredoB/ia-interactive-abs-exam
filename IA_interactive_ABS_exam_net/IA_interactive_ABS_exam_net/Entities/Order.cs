using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IA_interactive_ABS_exam_net.Entities
{
    public class Order
    {
        [Key]
        public int OrderID { get; set; }
        public DateTime TimeStamp { get; set; }
        [Required]
        public int OrderStatusID { get; set; }

        //Relation externals
        public OrderStatus OrderStatus { get; set; }
        public List<ProductOrder> ProductsOrder { get; set; }
    }
}