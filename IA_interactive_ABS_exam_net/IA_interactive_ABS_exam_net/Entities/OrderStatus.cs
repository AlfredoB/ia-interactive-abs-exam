using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;

namespace IA_interactive_ABS_exam_net.Entities
{
    public class OrderStatus
    {
        [Key]
        public int OrderStatusID { get; set; }
        public string Decription { get; set; }

        //Relation externals
       public List<Order> Order { get; set; }
       public List<ProductOrder> ProductsOrder { get; set; }
    }
}