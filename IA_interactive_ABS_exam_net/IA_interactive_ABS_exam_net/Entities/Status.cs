using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;

namespace IA_interactive_ABS_exam_net.Entities
{
    public class Status
    {
        [Key]
        public int StatusID { get; set; }
        public string Decription { get; set; }

        //Relation externals
        public List<ProductOrder> ProductsOrder { get; set; }
        public List<Product> Products { get; set; }
    }
}