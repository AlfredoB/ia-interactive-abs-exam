﻿using AutoMapper;
using IA_interactive_ABS_exam_net.Entities;
using IA_interactive_ABS_exam_net.Models;

namespace IA_interactive_ABS_exam_net.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<DtoProduct, Product>();
            CreateMap<Product, DtoProduct>()
                .ForMember(dest => dest.Status, o => o.MapFrom(src => src.Status.Decription));


            CreateMap<DtoOrder, Order>();
            CreateMap<Order, DtoOrder>()
                .ForMember(dest => dest.OrderStatus, o => o.MapFrom(src => src.OrderStatus.Decription));


            CreateMap<DtoProductOrder, ProductOrder>();
            CreateMap<ProductOrder,DtoProductOrder >()
                .ForMember(dest => dest.OrderStatus, o => o.MapFrom(src => src.OrderStatus.Decription))
                .ForMember(dest => dest.Product, o => o.MapFrom(src => src.Product.Decription));

            CreateMap<DtoOrderStatus, OrderStatus>();

        }
    }
}
