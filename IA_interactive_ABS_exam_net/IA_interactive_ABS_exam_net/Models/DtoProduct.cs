using System.ComponentModel.DataAnnotations;

namespace IA_interactive_ABS_exam_net.Models
{
    public class DtoProduct
    {
        public int ProductID { get; set; }
        public string SKU { get; set; }
        public string Decription { get; set; }
        public int Stock { get; set; }
        public DateTime TimeStamp { get; set; }
        public int StatusID { get; set; }

        //Aditionals
        public string? Status { get; set; }
    }
}