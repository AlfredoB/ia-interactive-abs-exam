namespace IA_interactive_ABS_exam_net.Models
{
    public class DtoOrder
    {
        public int OrderID { get; set; }
        public DateTime? TimeStamp { get; set; }
        public int OrderStatusID { get; set; }

        //Relation externals
        public string OrderStatus { get; set; }
        public List<DtoProductOrder> ProductsOrder { get; set; }
    }
}