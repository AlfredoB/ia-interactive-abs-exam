﻿const baseUrl = "http://localhost:5083/api/";

const OrderStatusID = {
    Pending: 1,
    InProcess: 2,
    Completed: 3,
    Delivered: 4,
    Canceled: 5,
}

const OrderStatus = ["Pending", "In Process", "Completed", "Delivered", "Canceled"]

const Routes = {
    Orders: "Order",
    Products: "Product",
}