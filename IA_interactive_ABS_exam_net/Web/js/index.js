﻿var ListProductsOrder = [];

$(document).ready(function () {
    $("#processing").show();
    //Wait for started api client
    setTimeout(function () {
        common.Call("GET", Routes.Orders + "/Status/1").then(function (response) {
            ShowOrders(response);
            $("#processing").hide();
        }).fail(() => {
            $("#processing").hide();
        });
    }, 8000);
})

ShowOrders = function (orders) {
    let app = $("#app");
    app.html(null);
    orders.forEach(function (o, i) {
        app.append(orderCard(o));
    });
}

OpenAddOrderModal = function () {
    $("#processing").show();
    common.Call("GET", Routes.Products).then(function (response) {
        let products = $("#ddProductOrder");
        products.html(null);

        response.forEach(function (p, i) {
            products.append(`<option value="${p.productID}">${p.decription}</option>`);
        });

        $("#AddOrderModal").modal('show');
        $("#processing").hide();
    }).fail(() => {
        alert("Fail to get list of Products");
        $("#processing").hide();
    });

    let tableProductsOrder = $("#tbProductsOrder").find("tbody");
    tableProductsOrder.html("");
    ListProductsOrder = [];
}

ChangeStept = function (orderId, orderStatusID) {
    let param = {
        orderId: orderId,
        orderStatusID: orderStatusID
    }

    $("#processing").show();
    common.Call("PUT", Routes.Orders + "/Status/", JSON.stringify(param)).then(function (response) {

        common.Call("GET", Routes.Orders + "/Status/" + (orderStatusID - 1)).then(function (response) {
            ShowOrders(response);
            alert("Order upated!");
            $("#processing").hide();
        }).fail(() => {
            alert("Fail to get list of Orders");
            $("#processing").hide();
        });

    }).fail(() => {
        alert("Fail to update Order");
        $("#processing").hide();
    });
}

AddProductToOrder = function () {
    let product = {
        productID: $("#ddProductOrder").val(),
        decription: $("#ddProductOrder option:selected").text(),
        count: $("#txtCountOrder").val(),
        orderStatusID: 1
    }

    if (ListProductsOrder.length > 0 && ListProductsOrder.findIndex(x => x.productID == product.productID) != -1) {
        alert("The product selected is exist in the order");
        return;
    }

    ListProductsOrder.push(product);

    let tableProductsOrder = $("#tbProductsOrder").find("tbody");

    tableProductsOrder.append(`<tr><td>${product.decription}</td>
                <td>${product.count}</td>
                <td><div class="btn btn-outline-danger" onclick="DeleteProductOrder(${product.productID})"><i class="fa fa-trash mr-1"></i></div></td>
    </tr>`);

    $("#ddProductOrder").val("");
    $("#txtCountOrder").val("");
}

DeleteProductOrder = function (productID) {
    let index = ListProductsOrder.findIndex(x => x.productID == productID);

    $("#tbProductsOrder tr").eq(index + 1).remove();
    ListProductsOrder.splice(index, 1);

}


CreateOrder = function () {
    if (ListProductsOrder.length == 0) {
        alert("There aren't any products in the new Order");
        return;
    }

    let param = {
        orderStatusID: 1,
        timestamp: null,
        ProductsOrder: ListProductsOrder
    }

    $("#processing").show();
    common.Call("POST", Routes.Orders, JSON.stringify(param)).then(function (response, status) {
        if (status == 202) {
            $("#processing").hide();
            alert(response);
            return;
        }

        common.Call("GET", Routes.Orders + "/Status/1").then(function (response) {
            ShowOrders(response);
            alert("Order created!");
            $("#AddOrderModal").modal('hide');
            ListProductsOrder = [];
            $("#processing").hide();
        }).fail(() => {
            alert("Fail to get list of Orders");
            $("#processing").hide();
        });

    }).fail(() => {
        alert("Fail to create Order");
        $("#processing").hide();
    });
}