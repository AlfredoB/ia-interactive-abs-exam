﻿var common = {
    Call: function (type, url, data, contentTypeSend, asyncSend) {
        var deferred = $.Deferred();
        let dataSend = data || {};
        let contentType = contentTypeSend || 'aplication/json; charset=utf-8';
        let async = asyncSend || true;

        $.ajax({
            type: type,
            url: baseUrl + url,
            data: dataSend,
            crossDomain: true,
            contentType: contentType,
            async: async,
            headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
            success: function (data, textStatus, xhr) {
                deferred.resolve(data, xhr.status);
            },
            error: function (e, a, b, c) {
                deferred.reject();
            }
        });

        return deferred.promise();
    }
}