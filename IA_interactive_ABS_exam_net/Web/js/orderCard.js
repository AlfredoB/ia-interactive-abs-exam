﻿orderCard = function (order) {

    let listProducts = ``;

    order.productsOrder.forEach(function (p, i) {
        listProducts += `<li class="list-group-item">${p.product} - ${p.count}`;
    });

    let actions = `<div class="action-next">
                    <div class="btn btn-outline-dark" onclick="ChangeStept(${order.orderID}, ${order.orderStatusID + 1})"><i class="fa fa-play mr-1"></i>${OrderStatus[order.orderStatusID]}</div>
                </div>
                <div class="action-cancel">
                    <div class="btn btn-outline-danger" onclick="ChangeStept(${order.orderID}, 5)"><i class="fa fa-trash mr-1"></i>Cancel</div>
                </div>`;

    if (order.orderStatusID == OrderStatusID.Delivered || order.orderStatusID == OrderStatusID.Canceled)
        actions = "";

    return `<div class="card">
            <div class="card-header">
                Order #${order.orderID}
                <div class="status" ${order.orderStatus.replace(" ", "")}>${order.orderStatus}</div>
            </div>
            <div class="card-body">
                <div class="products">
                    <span>Products</span>
                    <div class="lis-products">
                        <ul class="list-group">
                            ${listProducts}
                        </ul>
                    </div>
                </div>
                ${actions}
            </div>
        </div>`;
}